#!/bin/sh

#  RemoveDefaultApplications.sh
#
#
#  Created by David Kittell on 11/17/17.
#

# Games
sudo apt remove -y --purge python-pygame
sudo apt remove -y --purge zeitgeist
sudo apt remove -y --purge airstrike
sudo apt remove -y --purge aisleriot
sudo apt remove -y --purge ardour
sudo apt remove -y --purge brutalchess
sudo apt remove -y --purge warzone2100
sudo apt remove -y --purge mahjongg
sudo apt remove -y --purge gnome-games*
sudo apt remove -y --purge supertux*
sudo apt remove -y --purge xmoto
sudo apt remove -y --purge minetest
sudo apt remove -y --purge gnome-mahjongg
sudo apt remove -y --purge gnome-mines
sudo apt remove -y --purge gnome-sudoku
sudo apt remove -y --purge gweled
sudo apt remove -y --purge trigger-rally
sudo apt remove -y --purge armagetronad
sudo apt remove -y --purge neverball
sudo apt remove -y --purge neverputt
sudo apt remove -y --purge extremetuxracer
sudo apt remove -y --purge quadrapassel
sudo apt remove -y --purge pingus
sudo apt remove -y --purge gnomine
sudo apt remove -y --purge gnome-sudoku
sudo apt remove -y --purge gnome-mahjongg
sudo apt remove -y --purge gnome-sudoku
sudo apt remove -y --purge gnome-mines
sudo apt remove -y --purge gnome-mahjongg
sudo apt remove -y --purge xonotic
sudo apt remove -y --purge frozen-bubble

# Development
sudo apt remove -y --purge bluefish
sudo apt remove -y --purge gnome-builder

# Content Editors
sudo apt remove -y --purge gobby
sudo apt remove -y --purge calibre
sudo apt remove -y --purge glabels

# Torrent
sudo apt remove -y --purge transmission-common
sudo apt remove -y --purge transmission-gtk
sudo apt remove -y --purge transmission-*
sudo apt remove -y --purge kazam

# Audio / Visual
sudo apt remove -y --purge audacity # Audio Editing
sudo apt remove -y --purge blender # 3d Modeling
sudo apt remove -y --purge handbrake # Video Editing
sudo apt remove -y --purge cheese # WebCam software
sudo apt remove -y --purge kodi
sudo apt remove -y --purge inkscape # Adobe Illustrator replacement
sudo apt remove -y --purge gimp # Adobe Photoshop replacement
sudo apt remove -y --purge freecad
sudo apt remove -y --purge brasero
sudo apt remove -y --purge rhythmbox # Media Player
sudo apt remove -y --purge dia # Visio Replacement
sudo apt remove -y --purge gespeaker
sudo apt remove -y --purge rhythmbox
sudo apt remove -y --purge transmageddon # Video Format Conversion
sudo apt remove -y --purge lmms # Media Player
sudo apt remove -y --purge kdenlive # Video Editor

# Browser / Internet
sudo apt remove -y --purge thunderbird* # Email Client
sudo apt remove -y --purge firefox
sudo apt remove -y --purge chromium-browser
sudo apt remove -y --purge thunderbird # Email Client
sudo apt remove -y --purge *geary # Email Client
sudo apt remove -y --purge liferea # News Reader
sudo apt remove -y --purge empathy-common # Chat/Call Client
sudo apt remove -y --purge empathy* # Chat/Call Client
sudo apt remove -y --purge zorin-web-browser-manager

# Virtual PC
sudo apt remove -y --purge virtualbox

sudo apt remove -y --purge gnome-activity-journal

sudo apt remove -y --purge shotwell*
sudo apt remove -y --purge shotwell
sudo apt remove -y --purge totem*
sudo apt remove -y --purge account-plugin-*
sudo apt remove -y --purge friends*
sudo apt remove -y --purge unity-webapps-common
sudo apt remove -y --purge gnome-orca # Screen Reader
sudo apt remove -y --purge gnome-todo
sudo apt remove -y --purge gnome-weather
sudo apt remove -y --purge xournal
sudo apt remove -y --purge synergy
sudo apt remove -y --purge darktable
#sudo apt remove -y --purge vlc
sudo apt remove -y --purge vim
sudo apt remove -y --purge vym
sudo apt remove -y --purge librecad
sudo apt remove -y --purge mypaint
sudo apt remove -y --purge scribus
sudo apt remove -y --purge gnucash # Manage Finances
sudo apt remove -y --purge homebank # Manage Finances
sudo apt remove -y --purge planner # Project Management
sudo apt remove -y --purge referencer
sudo apt remove -y --purge gnome-dictionary
sudo apt remove -y --purge gnome-calendar
sudo apt remove -y --purge gnome-contacts
sudo apt remove -y --purge pitivi
sudo apt remove -y --purge mixxx
sudo apt remove -y --purge qjackctl
sudo apt remove -y --purge cellwriter
sudo apt remove -y --purge easystroke
sudo apt remove -y --purge *amazon*

sudo apt autoremove -y
sudo apt autoclean -y